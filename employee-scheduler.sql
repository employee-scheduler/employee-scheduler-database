CREATE DATABASE IF NOT EXISTS Employee_Scheduler;
USE Employee_Scheduler;

DROP TABLE IF EXISTS Company;
CREATE TABLE Company (
    company_ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` TINYTEXT NOT NULL,
    country TINYTEXT NOT NULL,
    region TINYTEXT NOT NULL,
    city TINYTEXT NOT NULL,
    `address` VARCHAR(500),
    area_code VARCHAR(16),
    time_format ENUM('12 HR', '24 HR'),
    PRIMARY KEY (company_ID)
);

DROP TABLE IF EXISTS Staff;
CREATE TABLE Staff (
    staff_ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    company_ID INT UNSIGNED NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    middle_name VARCHAR(255),
    last_name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    job_position VARCHAR(255) NOT NULL,
    admin_flag TINYINT UNSIGNED NOT NULL,
    deleted TINYINT UNSIGNED NOT NULL DEFAULT 0,
    min_weekly_hours TINYINT UNSIGNED,
    max_weekly_hours TINYINT UNSIGNED,
    min_weekly_shifts TINYINT UNSIGNED,
    max_weekly_shifts TINYINT UNSIGNED,
    schedule_colour VARCHAR(100) DEFAULT '#808080',
    PRIMARY KEY (staff_ID),
    FOREIGN KEY (company_ID) REFERENCES Company(company_ID),
    UNIQUE (email),
    CHECK (min_weekly_hours <= 168 AND min_weekly_hours <= max_weekly_hours),
    CHECK (max_weekly_hours <= 168 ),
    CHECK (min_weekly_shifts <= max_weekly_shifts)
);

DROP TABLE IF EXISTS Staff_Schedule;
CREATE TABLE Staff_Schedule (
    company_ID INT UNSIGNED NOT NULL,
    staff_ID INT UNSIGNED NOT NULL,
    `weekday` ENUM('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday') NOT NULL,
    `date` DATE NOT NULL,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
    FOREIGN KEY (company_ID) REFERENCES Company(company_ID),
    FOREIGN KEY (staff_ID) REFERENCES Staff(staff_ID)
);

DROP TABLE IF EXISTS Staff_Phone;
CREATE TABLE Staff_Phone (
    phone_ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    staff_ID INT UNSIGNED NOT NULL,
    phone_number VARCHAR(50) NOT NULL,
    phone_type ENUM('mobile', 'home', 'work', 'other') NOT NULL,
    phone_ext VARCHAR(50),
    PRIMARY KEY (phone_ID),
    FOREIGN KEY (staff_ID) REFERENCES Staff(staff_ID) ON DELETE CASCADE
);

DROP TABLE IF EXISTS Schedule_Preference;
CREATE TABLE Schedule_Preference (
    preference_ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    staff_ID INT UNSIGNED NOT NULL,
    preference TEXT NOT NULL,
    PRIMARY KEY (preference_ID),
    FOREIGN KEY (staff_ID) REFERENCES Staff(staff_ID) ON DELETE CASCADE
);

DROP TABLE IF EXISTS Staff_Availability;
CREATE TABLE Staff_Availability (
    availability_ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    staff_ID INT UNSIGNED NOT NULL,
    `weekday` ENUM('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday') NOT NULL,
    start_time TIME,
    end_time TIME,
    available_status ENUM('always', 'sometimes', 'never') NOT NULL,
    note TEXT,
    last_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (availability_ID),
    FOREIGN KEY (staff_ID) REFERENCES Staff(staff_ID) ON DELETE CASCADE
);

DROP TABLE IF EXISTS Schedule_Exception;
CREATE TABLE Schedule_Exception (
    exception_ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    staff_ID INT UNSIGNED NOT NULL,
    `date` DATE NOT NULL,
    `weekday` ENUM('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday') NOT NULL,
    start_time TIME,
    end_time TIME,
    is_available TINYINT UNSIGNED NOT NULL,
    note TEXT,
    PRIMARY KEY (exception_ID),
    FOREIGN KEY (staff_ID) REFERENCES Staff(staff_ID) ON DELETE CASCADE
);