INSERT INTO Company VALUES
(1, 'Corner Store', 'Canada', 'Newfoundland', "Turk's Gut", '25 Main Road', 'A0B 1C2', '12 HR');

INSERT INTO Staff VALUES 
(1, 1, 'John', null, 'Doe', 'jdoe@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Cashier', 0, 0, null, null, 3, null, '#808080'),
(2, 1, 'Doug', null, 'Dimmadome', 'ddimmadome@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Owner', 1, 0, null, null, null, null, '#808080'),
(3, 1, 'Moe', null, 'Howard', 'stooge1@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Security', 0, 0, 20, null, null, 5, '#000080'),
(4, 1, 'Curly', null, 'Stooge', 'stooge2@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Janitor', 0, 0, 30, null, null, null, '#cc3300'),
(5, 1, 'Larry', null, 'Howard', 'stooge3@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Clerk', 0, 0, 30, null, 6, null, '#808080'),
(6, 1, 'Abbott', null, 'Costello', 'acostello@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Cashier', 0, 0, null, null, 3, null, '#808080'),
(7, 1, 'Shemp', null, 'Howard', 'stooge4@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Cashier', 0, 0, null, 40, 4, null, '#808080'),
(8, 1, 'Baba', null, 'Ghanoush', 'hubbababa@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Cashier', 0, 0, 10, 60, 4, 6, '#3366cc'),
(9, 1, 'Peach', 'Ginger', 'Tee', 'pgtips@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Accountant', 0, 0, null, null, null, 4, '#800040'),
(10, 1, 'Jane', null, 'Doe', 'howwedoeit@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Clerk', 0, 0, null, null, 1, 3, '#808080'),
(11, 1, 'Nami', 'Ashley', 'Brown', 'nbrown@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Cashier', 0, 0, null, null, null, 3, '#999900'),
(12, 1, 'George', 'Louis', 'Costanza', 'artvandelay@test.com', '$2a$10$WicqAFoVh/H5BjGDNGSO.OQ6hvfjGEVVRGZlYS55rInx8SDG91Uva', 'Manager', 1, 0, null, null, null, null, '#808080');

INSERT INTO Staff_Phone VALUES
(1, 1, '12223334444', 'home', null),
(2, 2, '13334445555', 'work', '400'),
(3, 2, '12224445555', 'mobile', null),
(4, 2, '14442221111', 'home', null),
(5, 3, '13334445656', 'work', null),
(6, 3, '16664445555', 'mobile', null),
(7, 3, '16662221111', 'home', null),
(8, 4, '16664345454', 'mobile', null),
(9, 4, '16662227777', 'home', null),
(10, 5, '15551112222', 'home', null),
(11, 6, '12113331212', 'home', null),
(12, 6, '13334445555', 'work', '500'),
(13, 6, '13134883371', 'mobile', null),
(14, 7, '15558882222', 'home', null),
(15, 8, '14001117777', 'mobile', null),
(16, 9, '13334445555', 'work', '200'),
(17, 9, '19024443344', 'mobile', null),
(18, 9, '14442225678', 'home', null),
(19, 10, '12223334444', 'home', null),
(20, 11, '14567890001', 'mobile', null),
(21, 11, '12229996600', 'other', null),
(22, 12, '13334445555', 'work', '404'),
(23, 12, '12224440987', 'mobile', null),
(24, 12, '14442225467', 'home', null);

INSERT INTO Schedule_Preference VALUES 
(1, 1, 'Does not want to be scheduled with Jane Doe.'),
(2, 3, 'Does not want back-to-back shifts (ex: evening followed by morning).'),
(3, 4, 'Prefers morning shifts.'),
(4, 6, 'Prefers to have 3 days in a row.'),
(5, 7, 'Prefers evening shifts.'),
(6, 7, 'Does not want to be scheduled with Larry Howard.'),
(7, 10, 'Does not want two days in a row.'),
(8, 10, 'Would like to have weekends off.'),
(9, 11, 'Prefers being scheduled with Jane Doe.');